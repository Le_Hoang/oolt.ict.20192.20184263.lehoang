package Lab01;

import javax.swing.JOptionPane;
public class Solve_equations{

    public static void Solve_1st_deg_equation(){
        String strNum_a, strNum_b;
        double result;
        double dNum_a, dNum_b;

        strNum_a = JOptionPane.showInputDialog(null,"Please input a ","ax + b = 0",JOptionPane.INFORMATION_MESSAGE);
        
        dNum_a = Double.parseDouble(strNum_a);

        strNum_b = JOptionPane.showInputDialog(null,"Please input b ","ax + b = 0",JOptionPane.INFORMATION_MESSAGE);

        dNum_b = Double.parseDouble(strNum_b);

        result = -dNum_b/dNum_a;

        JOptionPane.showMessageDialog(null,"x = " + result, "Result: ", JOptionPane.INFORMATION_MESSAGE);
        
        System.exit(0);
    }

    public static void Solve_2nd_deg_equation (){
        String strNum_a, strNum_b,strNum_c;
        double result_x1, result_x2;
        double dNum_a, dNum_b, dNum_c,dNum_delta;

        while(true){
            strNum_a = JOptionPane.showInputDialog(null,"Please input a (not 0)","ax^2 + bx +c = 0",JOptionPane.INFORMATION_MESSAGE);
            dNum_a = Double.parseDouble(strNum_a);
            if (dNum_a == 0){
                JOptionPane.showMessageDialog(null, "please enter a different than 0", "Degree error!",JOptionPane.INFORMATION_MESSAGE);
                continue;
            }
            break;
        }

        strNum_b = JOptionPane.showInputDialog(null,"Please input b ","ax^2 + bx +c = 0",JOptionPane.INFORMATION_MESSAGE);

        dNum_b = Double.parseDouble(strNum_b);

        strNum_c = JOptionPane.showInputDialog(null,"Please input c ","ax^2 + bx +c = 0",JOptionPane.INFORMATION_MESSAGE);

        dNum_c = Double.parseDouble(strNum_c);

        dNum_delta = Math.pow(dNum_b,2) - 4*dNum_a*dNum_c;

        if (dNum_delta == 0)
        {
            result_x1 = -(dNum_b)/2*dNum_a;
            JOptionPane.showMessageDialog(null, "x1 = x2 = " + result_x1, "Result", JOptionPane.INFORMATION_MESSAGE);
        }else if (dNum_delta >0)
        {
            result_x1 = (-dNum_b + Math.sqrt(dNum_delta))/2*dNum_a;
            result_x2 = (-dNum_b - Math.sqrt(dNum_delta))/2*dNum_a;
            JOptionPane.showMessageDialog(null,"x1 = " + result_x1 +", x2 = " + result_x2,"Result",JOptionPane.INFORMATION_MESSAGE);
        }else{
            JOptionPane.showMessageDialog(null,"The equation has no real root", "Result: ", JOptionPane.INFORMATION_MESSAGE);
        }
        
        System.exit(0);

    }
    
    public static void Solve_system_1st_deg_equation(){
        String strNum_a11, strNum_a12,strNum_a21,strNum_a22,strNum_b1,strNum_b2;
        double dNum_a11, dNum_a12, dNum_a21,dNum_a22,dNum_b1,dNum_b2,D,D1,D2,result_x1 = 0,result_x2 = 0;

        strNum_a11 = JOptionPane.showInputDialog(null,"Please input a11 ","a11*x1 + a12*x2 = b1 \na21*x1 + a22*x2 = b2 ",JOptionPane.INFORMATION_MESSAGE);
        
        dNum_a11 = Double.parseDouble(strNum_a11);

        strNum_a12 = JOptionPane.showInputDialog(null,"Please input a12 ","a11*x1 + a12*x2 = b1 \na21*x1 + a22*x2 = b2 ",JOptionPane.INFORMATION_MESSAGE);
        
        dNum_a12 = Double.parseDouble(strNum_a12);

        strNum_b1 = JOptionPane.showInputDialog(null,"Please input b1 ","a11*x1 + a12*x2 = b1 \na21*x1 + a22*x2 = b2 ",JOptionPane.INFORMATION_MESSAGE);
        
        dNum_b1 = Double.parseDouble(strNum_b1);

        strNum_a21 = JOptionPane.showInputDialog(null,"Please input a21 ","a11*x1 + a12*x2 = b1 \na21*x1 + a22*x2 = b2 ",JOptionPane.INFORMATION_MESSAGE);
        
        dNum_a21 = Double.parseDouble(strNum_a21);

        strNum_a22 = JOptionPane.showInputDialog(null,"Please input a22 ","a11*x1 + a12*x2 = b1 \na21*x1 + a22*x2 = b2 ",JOptionPane.INFORMATION_MESSAGE);
        
        dNum_a22 = Double.parseDouble(strNum_a22);

        strNum_b2 = JOptionPane.showInputDialog(null,"Please input b2 ","a11*x1 + a12*x2 = b1 \na21*x1 + a22*x2 = b2 ",JOptionPane.INFORMATION_MESSAGE);
        
        dNum_b2 = Double.parseDouble(strNum_b2);

        D = dNum_a11*dNum_a22 - dNum_a21*dNum_a12;
        D1 = dNum_b1*dNum_a22 - dNum_b2*dNum_a12;
        D2 = dNum_a11*dNum_b2 - dNum_a21*dNum_b1;

        if (D != 0){
            
            result_x1 = D1/D;
            result_x2 = D2/D;
            JOptionPane.showMessageDialog(null,"x1 = " + result_x1 + "x2 = " + result_x2, "Result: ", JOptionPane.INFORMATION_MESSAGE);
        } else if(D == 0 && D1 == 0 && D2 == 0) {
            JOptionPane.showMessageDialog(null,"System has infinite solution ", "Result: ", JOptionPane.INFORMATION_MESSAGE);
        } else {
            JOptionPane.showMessageDialog(null,"System has no solution " , "Result: ", JOptionPane.INFORMATION_MESSAGE);
        }
        System.exit(0);
    }
    public static void main (String[] args){
        String strOption;
        strOption = JOptionPane.showInputDialog(null, " 1.Solve 1st degree equation\n 2. Solve 2nd degree equation\n 3. Solve system of two 1st degree equations ", "Choose one of these", JOptionPane.INFORMATION_MESSAGE);

        switch(strOption){
            case "1" : Solve_1st_deg_equation();break;
            case "2" : Solve_2nd_deg_equation();break;
            case "3" : Solve_system_1st_deg_equation();break;
            default: System.exit(0);
        }
    }
}