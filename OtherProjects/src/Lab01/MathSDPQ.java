package Lab01;

import javax.swing.JOptionPane;
public class MathSDPQ{
    public static void main (String[] args){
        String strNum1, strNum2;
        double sum,difference,product,quotient;
        double dNum1, dNum2;

        strNum1 = JOptionPane.showInputDialog(null,"Please input the first number","Input first number",JOptionPane.INFORMATION_MESSAGE);
        
        dNum1 = Double.parseDouble(strNum1);

        strNum2 = JOptionPane.showInputDialog(null,"Please input the second number","Input second number",JOptionPane.INFORMATION_MESSAGE);

        dNum2 = Double.parseDouble(strNum2);

        sum = Double.sum(dNum1, dNum2) ;
        difference = Math.abs(dNum1 - dNum2);
        product = dNum1*dNum2;
        if (dNum2 != 0){
            quotient = dNum1/dNum2;
        } else quotient = 0;

        JOptionPane.showMessageDialog(null,"num1 + num2 = " + sum, "Sum", JOptionPane.INFORMATION_MESSAGE);
        JOptionPane.showMessageDialog(null,"|num1 - num2| = " + difference, "Difference", JOptionPane.INFORMATION_MESSAGE);
        JOptionPane.showMessageDialog(null,"num1 * num2 = " + product, "Product", JOptionPane.INFORMATION_MESSAGE);
        
        if (dNum2 != 0){
            JOptionPane.showMessageDialog(null,"num1 / num2 = " + quotient, "Quotient", JOptionPane.INFORMATION_MESSAGE);
        } else {
            JOptionPane.showMessageDialog(null,"divisor is 0, can't find quotient", "Quotient", JOptionPane.INFORMATION_MESSAGE);
        }

        System.exit(0);
    }
}