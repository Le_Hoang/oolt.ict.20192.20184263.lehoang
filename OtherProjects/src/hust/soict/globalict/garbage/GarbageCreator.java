package hust.soict.globalict.garbage;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.Map;
import java.util.Random;

public class GarbageCreator {
	public static void main(String[] args) {
		
		// Unimproved version
		// Garbage collector auto collect garbage in heap when it reaches 232/274M
		// Program still run without stopping
		
		
//		String s = "";
//		File file = new File("E:\\Hoang\\Schoolwork\\2019 Term 2\\OOP - Java\\Lab03\\Eclipse's work\\OtherProjects\\TextFiles\\TestText01.txt");
//		
//		System.out.println("Start creating garbage");
//		
//		try (BufferedReader br = new BufferedReader(new FileReader(file))) {
//		    String line;
//		    while ((line = br.readLine()) != null) {
//		       s = s + line;
//		       System.out.println("..");
//		    }
//		} 
//		catch (FileNotFoundException e)  {e.printStackTrace();}
//		catch (IOException e) {e.printStackTrace();}
//		System.out.println("Garbage created");
//		
		
		// Possibly improved version
		
//		String s2 = "";
//		
//		StringBuffer sbf = new StringBuffer();
//		
//		try (BufferedReader br = new BufferedReader(new FileReader(file))) {
//		    String line;
//		    while ((line = br.readLine()) != null) {
//		       sbf.append(line);
//		       System.out.println("..");
//		    }
//		} 
//		catch (FileNotFoundException e)  {e.printStackTrace();}
//		catch (IOException e) {e.printStackTrace();}
//		
//		s2 = sbf.toString();
//		System.out.println("Garbage created");
		
		
		
			//this causes Java to stop from garbage overloading
	    Map map = System.getProperties();
	    Random r = new Random();
	    while (true) {
	      map.put(r.nextInt(), "value");
	    }
			  
			
		
	}
}
