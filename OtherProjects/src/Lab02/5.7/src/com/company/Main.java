package com.company;

import java.util.Scanner;
public class Main {

    public static void main(String[] args) {
        Scanner keyboard = new Scanner(System.in);
        int matrixSize;

        System.out.println("Put in matrix size: ");
        matrixSize = keyboard.nextInt();

        int[][] matrixA = new int[matrixSize][matrixSize], matrixB = new int[matrixSize][matrixSize];

        getMatrix(matrixA, "A", keyboard);
        getMatrix(matrixB, "B", keyboard);

        printMatrix(matrixA,"A");
        printMatrix(matrixB,"B");

        addMatrix(matrixA,matrixB);

    }

    public static void getMatrix(int[][] matrix, String matrixName, Scanner keyboard){
        System.out.println("Enter matrix " + matrixName + ": ");
        for(int i=0; i<matrix.length; i++){
            for(int j=0; j<matrix[i].length; j++){
                System.out.print("Put in "+ matrixName +" [" + (i+1) + "][" + (j+1) + "]: ");
                matrix[i][j] = keyboard.nextInt();
            }
        }
    }

    public static void printMatrix(int[][] matrix,String matrixName){
        System.out.println(matrixName + " = [");
        for(int i=0; i<matrix.length; i++){
            for(int j=0; j<matrix[i].length; j++){
                System.out.print(" " + matrix[i][j]);
            }
            System.out.println("");
        }
        System.out.println("]\n");
    }

    public static void addMatrix(int[][] matrixA, int[][] matrixB)
    {
        int[][] matrixSum = new int[matrixA.length][matrixA.length];
        for(int i=0; i<matrixA.length; i++){
            for(int j=0; j<matrixA[i].length; j++){
                matrixSum[i][j] = matrixA[i][j] + matrixB[i][j];
            }
        }

        printMatrix(matrixSum,"Sum");
    }
}
