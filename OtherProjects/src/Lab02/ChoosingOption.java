package Lab02;

import javax.swing.JOptionPane;
public class ChoosingOption{
    public static void main(String[] args){
        int option = JOptionPane.showConfirmDialog(null, "Do you want to change the first class ticket","Select Option",JOptionPane.YES_NO_OPTION, JOptionPane.INFORMATION_MESSAGE);

        Object[] options = {"Sure"}; 

        JOptionPane.showOptionDialog(null, "Are you really sure?","Select Option",JOptionPane.PLAIN_MESSAGE,JOptionPane.QUESTION_MESSAGE,null,options,options[0]);

        JOptionPane.showMessageDialog(null, "You've chosen " + (option ==JOptionPane.YES_OPTION ?"Yes":"No"));

        System.exit(0);
    }
}