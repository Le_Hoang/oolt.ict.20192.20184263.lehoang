package com.company;
import java.util.Scanner;
public class Main {

    public static void main(String[] args) {
        Scanner keyboard = new Scanner(System.in);
        int arraySize;

        System.out.println("Enter size of array: ");
        arraySize = keyboard.nextInt();

        int[] array = new int[arraySize];

        for (int i=0; i<arraySize; i++){
            System.out.print("Type in a[" + i + "]: ");
            array[i] = keyboard.nextInt();
        }

        System.out.println("Array before sort: ");
        printArray(array);

        sortArray(array);

        System.out.println("Array after sort: ");
        printArray(array);

    }

    public static void sortArray(int array[])
    {
        int n = array.length;
        for (int i = 1; i < n; ++i) {
            int key = array[i];
            int j = i - 1;

            while (j >= 0 && array[j] > key) {
                array[j + 1] = array[j];
                j = j - 1;
            }
            array[j + 1] = key;
        }
    }

    public static void printArray(int array[])
    {
        int n = array.length;
        for (int i = 0; i < n; ++i)
            System.out.print(array[i] + " ");

        System.out.println();
    }
}
