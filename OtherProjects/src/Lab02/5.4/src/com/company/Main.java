

import java.util.Scanner;
public class Main {

    public static void main(String[] args) {
        int iNum_n, iNum_l, iNum_r;
        int i;
        Scanner keyboard = new Scanner(System.in);

        System.out.print("Enter number n: ");

        iNum_n = keyboard.nextInt();
        iNum_l = iNum_n;
        iNum_r = iNum_n;

        while(true){
            for(i = 0; i < 2*iNum_n; i++){
                if (i >=iNum_l && i<= iNum_r){
                    System.out.print("*");
                } else {
                    System.out.print(" ");
                }
            }
            System.out.println("");
            iNum_l -= 1;
            iNum_r += 1;
            if(iNum_l <=0){
                break;
            }
        }

    }
}
