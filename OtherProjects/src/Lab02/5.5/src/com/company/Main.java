package com.company;

import java.util.Scanner;
public class Main {

    public static void main(String[] args) {
        String[] Jan = {"January", "Jan", "Jan.", "1"},
                Feb = {"February", "Feb", "Feb.", "2"},
                Mar = {"March", "Mar", "Mar.", "3"},
                Apr = {"April", "Apr", "Apr.", "4"},
                May = {"May", "May.", "5"},
                June = {"June", "Jun", "Jun.", "6"},
                July = {"July", "Jul", "Jul.", "7"},
                Aug = {"August", "Aug", "Aug.", "8"},
                Sep = {"September", "Sep", "Sep.", "9"},
                Oct = {"October", "Oct", "Oct.", "10"},
                Nov = {"November", "Nov", "Nov.", "11"},
                Dec = {"December", "Dec", "Dec.", "12"};

        String Option;
        Boolean month_isFound = false;
        int enteredMonth = 0;
        int enteredYear = -1;
        Scanner keyboard = new Scanner(System.in);

        while (true) {
            System.out.println("Put in month: ");
            Option = keyboard.nextLine();

            for (String month : Jan) {
                if (Option.toUpperCase().equals(month.toUpperCase())) {
                    month_isFound = true;
                    enteredMonth = 1;
                }
            }
            if (month_isFound) break;

            for (String month : Feb) {
                if (Option.toUpperCase().equals(month.toUpperCase())) {
                    month_isFound = true;
                    enteredMonth = 2;
                }
            }
            if (month_isFound) break;

            for (String month : Mar) {
                if (Option.toUpperCase().equals(month.toUpperCase())) {
                    month_isFound = true;
                    enteredMonth = 3;
                }
            }
            if (month_isFound) break;

            for (String month : Apr) {
                if (Option.toUpperCase().equals(month.toUpperCase())) {
                    month_isFound = true;
                    enteredMonth = 4;
                }
            }
            if (month_isFound) break;

            for (String month : May) {
                if (Option.toUpperCase().equals(month.toUpperCase())) {
                    month_isFound = true;
                    enteredMonth = 5;
                }
            }
            if (month_isFound) break;

            for (String month : June) {
                if (Option.toUpperCase().equals(month.toUpperCase())) {
                    month_isFound = true;
                    enteredMonth = 6;
                }
            }
            if (month_isFound) break;

            for (String month : July) {
                if (Option.toUpperCase().equals(month.toUpperCase())) {
                    month_isFound = true;
                    enteredMonth = 7;
                }
            }
            if (month_isFound) break;

            for (String month : Aug) {
                if (Option.toUpperCase().equals(month.toUpperCase())) {
                    month_isFound = true;
                    enteredMonth = 8;
                }
            }
            if (month_isFound) break;

            for (String month : Sep) {
                if (Option.toUpperCase().equals(month.toUpperCase())) {
                    month_isFound = true;
                    enteredMonth = 9;
                }
            }
            if (month_isFound) break;

            for (String month : Oct) {
                if (Option.toUpperCase().equals(month.toUpperCase())) {
                    month_isFound = true;
                    enteredMonth = 10;
                }
            }
            if (month_isFound) break;

            for (String month : Nov) {
                if (Option.toUpperCase().equals(month.toUpperCase())) {
                    month_isFound = true;
                    enteredMonth = 11;
                }
            }
            if (month_isFound) break;

            for (String month : Dec) {
                if (Option.toUpperCase().equals(month.toUpperCase())) {
                    month_isFound = true;
                    enteredMonth = 12;
                }
            }
            if (month_isFound) {
                break;
            } else {
                System.out.println("Please input again!");
            }
        }

        while (true) {
            System.out.println("Enter year: ");
            Option = keyboard.nextLine();

            try {
                enteredYear = Integer.parseInt(Option);
            } catch (Exception NumberFormatException) {
                System.out.println("Invalid form! Please enter a number greater than 0");
                continue;
            }

            if (enteredYear < 0) {
                System.out.println("Invalid form! Please enter a number greater than 0");
                continue;
            }
            break;
        }

        System.out.print(enteredMonth + "/" + enteredYear);


        switch (enteredMonth) {
            case 1:
            case 3:
            case 5:
            case 7:
            case 8:
            case 10:
            case 12:
                System.out.println(" has 31 days");
                break;
            case 4:
            case 6:
            case 9:
            case 11:
                System.out.println(" has 30 days");
                break;
            case 2: {
                if (enteredYear % 4 == 0) {
                    if (enteredYear % 100 == 0) {
                        if(enteredYear % 400 ==0){
                            System.out.println(" has 29 days");
                        } else{
                            System.out.println(" has 28 days");
                        }
                    } else{
                        System.out.println(" has 29 days");
                    }
                }else{
                    System.out.println(" has 28 days");
                }
                break;
            }
            default:break;
        }
        System.exit(0);
    }
}
