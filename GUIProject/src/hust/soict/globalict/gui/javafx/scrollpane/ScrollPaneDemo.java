package hust.soict.globalict.gui.javafx.scrollpane;

import javafx.application.Application;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.ScrollPane;
import javafx.scene.control.ScrollPane.ScrollBarPolicy;
import javafx.stage.Stage;

public class ScrollPaneDemo extends Application{

	@Override
	public void start(Stage primaryStage) {
		// Create a ScrollPane
        ScrollPane scrollPane = new ScrollPane();
 
        Button button = new Button("My Button");
        button.setPrefSize(400, 300);
        
        scrollPane.setContent(button);
        
        scrollPane.setVbarPolicy(ScrollBarPolicy.AS_NEEDED);
        
        scrollPane.setHbarPolicy(ScrollBarPolicy.ALWAYS);
        
        primaryStage.setTitle("ScrollPane Demo 1 (o7planning.org)");
        Scene scene = new Scene(scrollPane, 550, 200);
        primaryStage.setScene(scene);
        primaryStage.show();
	}
	
	 public static void main(String[] args) {
	        launch(args);
	    }
	
}
