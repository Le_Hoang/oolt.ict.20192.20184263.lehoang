package hust.soict.globalict.gui.javafx.effect;

import javafx.application.Application;
import javafx.scene.Group;
import javafx.scene.Scene;
import javafx.scene.effect.ColorAdjust;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.stage.Stage;
 
public class ColorAdjustEffectExample extends Application {
 
   @Override
   public void start(Stage stage) {
 
      String imageUrl = "/hust/soict/globalict/gui/javafx/menu/build-16.jpg";
      Image image = new Image(imageUrl);
 
      ImageView imageView1 = new ImageView(image);
      imageView1.setX(0);
      imageView1.setY(20);
 
      ImageView imageView2 = new ImageView(image);
 
      imageView2.setX(450);
      imageView2.setY(20);
 
      // Create the ColorAdjust
      ColorAdjust colorAdjust = new ColorAdjust();
 
      // Setting the contrast value
      colorAdjust.setContrast(0);
 
      // Setting the hue value
      colorAdjust.setHue(-5);
 
      // Setting the brightness value
      colorAdjust.setBrightness(0.05);
 
      // Setting the saturation value
      colorAdjust.setSaturation(-10);
 
      // Applying ColorAdjust effect to the ImageView node
      imageView2.setEffect(colorAdjust);
 
      Group root = new Group(imageView1, imageView2);
 
      Scene scene = new Scene(root, 1000, 400);
      stage.setTitle("JavaFX ColorAdjust Effect (o7planning.org)");
      stage.setScene(scene);
      stage.show();
   }
 
   public static void main(String args[]) {
      launch(args);
   }
 
}
