package hust.soict.globalict.gui.javafx.tableview;

import hust.soict.globalict.gui.javafx.models.UserAccount;
import javafx.application.Application;
import javafx.geometry.Insets;
import javafx.scene.Scene;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.layout.StackPane;
import javafx.stage.Stage;

public class TableViewDemo extends Application{

	@Override
	public void start(Stage stage) throws Exception {
		TableView<UserAccount> table = new TableView<UserAccount>();
		
		 TableColumn<UserAccount, String> userNameCol //
         = new TableColumn<UserAccount, String>("User Name");
		 
		 TableColumn<UserAccount, String> emailCol//
         = new TableColumn<UserAccount, String>("Email");
		 
		 TableColumn<UserAccount, String> fullNameCol//
         = new TableColumn<UserAccount, String>("Full Name");
		 
		 TableColumn<UserAccount, String> firstNameCol //
         = new TableColumn<UserAccount, String>("First Name");
		 
		 TableColumn<UserAccount, String> lastNameCol //
         = new TableColumn<UserAccount, String>("Last Name");
		 
		 fullNameCol.getColumns().addAll(firstNameCol, lastNameCol);
		 
		 TableColumn<UserAccount, Boolean> activeCol//
         = new TableColumn<UserAccount, Boolean>("Active");
		 
		 table.getColumns().addAll(userNameCol, emailCol,fullNameCol,activeCol);
		 
		 StackPane root = new StackPane();
	      root.setPadding(new Insets(5));
	      root.getChildren().add(table);
	      
	     stage.setTitle("TableView (o7planning.org)");
	     
	     Scene scene = new Scene(root, 450, 300);
	     stage.setScene(scene);
	     stage.show();
	}
	public static void main(String[] args) {
	      launch(args);
	  }

}
