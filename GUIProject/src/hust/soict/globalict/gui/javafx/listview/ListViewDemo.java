package hust.soict.globalict.gui.javafx.listview;

import hust.soict.globalict.gui.javafx.models.Book;
import javafx.application.Application;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.scene.Scene;
import javafx.scene.control.ListView;
import javafx.scene.control.SelectionMode;
import javafx.scene.layout.StackPane;
import javafx.stage.Stage;

public class ListViewDemo extends Application{

	@Override
	public void start(Stage stage) throws Exception {
	 Book book1 = new Book(1L, "J01", "Java IO Tutorial");
     Book book2 = new Book(2L, "J02", "Java Enums Tutorial");
     Book book3 = new Book(2L, "C01", "C# Tutorial for Beginners");
     
     ObservableList<Book> books = FXCollections.observableArrayList(book1, book2, book3);
     
     ListView<Book> listView = new ListView<Book>(books);
     
     listView.getSelectionModel().setSelectionMode(SelectionMode.MULTIPLE);
     
     listView.getSelectionModel().selectIndices(1);
     
     listView.getFocusModel().focus(2);
     
     StackPane root = new StackPane();
     root.getChildren().add(listView);
     
     stage.setTitle("ListView (o7planning.org)");
     
     Scene scene = new Scene(root, 350, 200);
     stage.setScene(scene);
     stage.show();
	}
	
	 public static void main(String[] args) {
	      launch(args);
	  }
}
